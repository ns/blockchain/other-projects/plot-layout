import os

from src import (BlockBar, BlocksDelayPlot, BlocksTxDistPlot, MempoolUsagePlot,
                 TransactionFeerateBlockchainPlot, TxIntercPlot)


def amount_of_blocks_and_transactions_plot():
    print('>>> amount_of_blocks_and_transactions_plot')
    plot = BlocksTxDistPlot()
    plot.plot()


def mempool_usage():
    print('>>> mempool_usage')
    plot = MempoolUsagePlot()
    plot.plot()


def block_delay():
    print('>>> block_delay')
    plot = BlocksDelayPlot()
    plot.plot()


def transaction_intersection():
    print('>>> transaction_intersection')
    plot = TxIntercPlot()
    plot.plot()


def transaction_feerate_blockchain():
    print('>>> transaction_feerate_blockchain')
    plot = TransactionFeerateBlockchainPlot()
    plot.plot()


# Synthetic plots
def block_bar():
    print('>>> block_bar')
    plot = BlockBar()
    plot.plot()


if __name__ == "__main__":
    os.makedirs('plots', exist_ok=True)
    amount_of_blocks_and_transactions_plot()
    block_bar()
    mempool_usage()
    block_delay()
    transaction_intersection()
    transaction_feerate_blockchain()
