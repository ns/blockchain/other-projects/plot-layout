import pandas as pd

from .utils import *


class MempoolUsagePlot:

    def __init__(self):
        self.filename_dataset = 'data/mempool_usage.csv.gz'
        self.sep = ';'
        self.compression = 'gzip'
        self.usecols = None
        self.parse_dates = ['creation_time']
        self.filename_plot = 'plots/mpool-sz-a-cdf.pdf'

    def plot(self):
        plt.clf()
        df = self.__load_dataframe()
        fig, ax = plt.subplots(nrows=1, ncols=1)

        df['mempool_bytes'] = df['mempool_bytes']/1e6

        ax = plot_cdf(
            df['mempool_bytes'], x_label='Mempool size (in MB)', y_label='CDF', interval=2)

        ax.axvline(x=1, ymin=0, ymax=1,
                   color=colors['red'], linewidth=2, linestyle='--')
        plt.text(0.3, 0.5, 'Block limit', rotation=90,
                 color=colors['red'])

        ax.xaxis.set_minor_locator(ticker.MultipleLocator(1))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.savefig(self.filename_plot,
                    bbox_inches='tight')

    def __load_dataframe(self):
        df = pd.read_csv(self.filename_dataset, sep=self.sep,
                         compression=self.compression, usecols=self.usecols, parse_dates=self.parse_dates)
        df.set_index('creation_time', inplace=True)
        return df
