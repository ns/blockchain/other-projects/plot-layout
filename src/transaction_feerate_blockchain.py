import gzip
import json

import numpy as np

from .utils import *


class TransactionFeerateBlockchainPlot:

    def __init__(self):
        self.filename_dataset = 'data/blockchain-tx-fee-vsize-miner.csv.gz'
        self.filename_plot = 'plots/blockchain-txs-feerate-cdf.pdf'
        self.filename_mpo_plot = 'plots/blockchain-txs-feerate-mp5-cdf.pdf'
        self.top_miners = ['BTC.com', 'AntPool',
                           'F2Pool', 'Poolin.com', 'SlushPool']

    def plot(self):
        data = self.__load_dataset()
        data['feesize'] = (data['tx_fee'])/(data['vsize']*1e5)
        self.__plot_all(data)
        self.__plot_mpo(data)

    def __plot_all(self, data):
        plt.clf()
        fig, ax = plt.subplots(nrows=1, ncols=1)

        ax = plot_cdf(
            data=data['feesize'], x_label='Transaction feerate (BTC/kB)', y_label='CDF', log=True)

        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))
        ax.set_xlim((1e-6, 10))
        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.savefig(self.filename_plot,
                    bbox_inches='tight')

    def __plot_mpo(self, data):
        fig, ax = plt.subplots(nrows=1, ncols=1)

        line_widths = [3, 3.5, 4, 4.5, 5]

        for miner in self.top_miners:
            ax = plot_cdf(
                data.query(
                    'miner == @miner').feesize, x_label='Transaction feerate (BTC/kB)',
                y_label='CDF', label=miner, ax=ax, linewidth=line_widths.pop(), log=True)

        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.legend()
        plt.savefig(self.filename_mpo_plot,
                    bbox_inches='tight')

    def __load_dataset(self):
        data = pd.read_csv(self.filename_dataset, sep=';', compression='gzip')
        return data
