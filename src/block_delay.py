import pandas as pd

from .utils import *


class BlocksDelayPlot:

    def __init__(self):
        self.filename_dataset = 'data/block_delay.csv.gz'
        self.sep = ';'
        self.compression = 'gzip'
        self.usecols = None
        self.parse_dates = ['tx_received_datetime']
        self.filename_plot = 'plots/cdf_tx_block_delay_log.pdf'

    def plot(self):
        plt.clf()
        df = self.__load_dataframe()

        ax = plot_cdf(df['tx_block_delay'],
                      x_label='Delay (in blocks)', y_label='CDF', log=True)

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='y', linestyle=':')

        ax.axvline(x=144, ymin=0, ymax=1,
                   color=colors['red'], linewidth=2, linestyle='--')
        plt.text(112, 0.77, '1 day', rotation=90,
                 color=colors['red'])

        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        for axis in [ax.xaxis]:
            formatter = ticker.ScalarFormatter()
            formatter.set_scientific(False)
            axis.set_major_formatter(formatter)

        ax.tick_params(which='minor')
        ax.tick_params(which='major')

        plt.savefig(self.filename_plot,
                    bbox_inches='tight')

    def __load_dataframe(self):
        df = pd.read_csv(self.filename_dataset, sep=self.sep,
                         compression=self.compression, usecols=self.usecols, parse_dates=self.parse_dates)
        return df
