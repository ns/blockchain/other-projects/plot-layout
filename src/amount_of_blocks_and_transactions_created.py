import pandas as pd

from .utils import *


class BlocksTxDistPlot:

    def __init__(self):
        self.filename_dataset = 'data/block-tx-coinbase-dist.csv.gz'
        self.sep = ';'
        self.compression = 'gzip'
        self.usecols = None
        self.parse_dates = ['timestamp']
        self.filename_plot = 'plots/amount_of_blocks_and_transactions_created.pdf'

    def plot(self):
        plt.clf()
        df = self.__load_dataframe()
        x = {'label': ''}
        y = {'label': 'CDF'}
        data = df.loc['2009':'2019-01-01']

        data_blocks = data.block_counter/data.block_counter.sum()
        data_blocks.index = data_blocks.index.date
        ax = cum_dist(data=data_blocks, x=x, y=y,
                      label='# blocks',  linewidth=2, color=colors['blue'], style='--')

        data_txs = data.n_tx/data.n_tx.sum()
        data_txs.index = data_txs.index.date
        ax = cum_dist(data=data_txs, x=x, y=y, ax=ax,
                      label='# transactions', linewidth=3, color=colors['red'], style='-')

        ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y'))
        ax.xaxis.set_major_locator(mdates.YearLocator())
        ax.xaxis.set_minor_locator(mdates.MonthLocator(interval=6))

        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        ax.legend(loc='best', fancybox=True, framealpha=0.5)
        plt.xlim(('2009', '2019'))

        plt.ylim((0, 1))

        plt.xticks(rotation=30, ha="center")

        plt.savefig(
            self.filename_plot, bbox_inches='tight')

    def __load_dataframe(self):
        df = pd.read_csv(self.filename_dataset, sep=self.sep,
                         compression=self.compression, usecols=self.usecols, parse_dates=self.parse_dates)
        df.set_index('timestamp', inplace=True)
        df['block_counter'] = 1
        return df
