import pandas as pd

from .utils import *


class TxIntercPlot:

    def __init__(self):
        self.filename_dataset = 'data/tx_intersection.csv.gz'
        self.sep = ';'
        self.compression = 'gzip'
        self.usecols = ['block_height', 'tx_intersection',
                        'congestion', 'block_miner', 'block_height']
        self.parse_dates = False
        self.filename_plot = 'plots/txs_intersection_cdf.pdf'
        self.filename_plot_all = 'plots/txs_intersection_all_cdf.pdf'
        self.filename_top_mpo_plot = 'plots/top_5_txs_intersection_cdf.pdf'
        self.filename_top_mpo_scatter_plot = 'plots/top_5_txs_intersection_dist.pdf'

    def plot(self):
        plt.clf()
        self.plot_single()
        self.plot_all()
        self.plot_top_5_mpo()
        self.plot_top_mpo_scatter()

    def plot_all(self):
        df = self.__load_dataframe()
        fig, ax = plt.subplots(nrows=1, ncols=1)

        # ax = plot_cdf(
        #    df.tx_intersection, x_label=r'$\mid B_i \cap \hat{B}_i \mid$', y_label='CDF', label='All', linewidth=4, color=colors['blue'])
        ax = plot_cdf(df.query('congestion == 1').tx_intersection,
                      x_label=r'$\mid B_i \cap \hat{B}_i \mid$', y_label='CDF', label='Congested', linewidth=4, ax=ax)
        ax = plot_cdf(df.query('congestion == 0').tx_intersection,
                      x_label=r'$\mid B_i \cap \hat{B}_i \mid$', y_label='CDF', label='Non-congested', linewidth=3, ax=ax)

        ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.legend()
        plt.savefig(self.filename_plot_all,
                    bbox_inches='tight')

    def plot_single(self):
        df = self.__load_dataframe()
        fig, ax = plt.subplots(nrows=1, ncols=1)

        ax = plot_cdf(
            df.tx_intersection, x_label=r'$\mid B_i \cap \hat{B}_i \mid$', y_label='CDF')

        ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.savefig(self.filename_plot,
                    bbox_inches='tight')

    def plot_top_5_mpo(self):
        df = self.__load_dataframe()
        top_mpo = df.block_miner.value_counts().index.values[:5]

        fig, ax = plt.subplots(nrows=1, ncols=1)

        line_widths = [3, 3.5, 4, 4.5, 5]

        for miner in top_mpo:
            ax = plot_cdf(
                df.query(
                    'block_miner == @miner').tx_intersection, x_label=r'$\mid B_i \cap \hat{B}_i \mid$',
                y_label='CDF', label=miner, ax=ax, linewidth=line_widths.pop())

        ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.legend()
        plt.savefig(self.filename_top_mpo_plot,
                    bbox_inches='tight')

    def plot_top_mpo_scatter(self):
        df = self.__load_dataframe()
        top_mpo = df.block_miner.value_counts().index.values[:5]

        fig, ax = plt.subplots(nrows=1, ncols=1)

        markers = ['P', 's', 'X', 'o', 'D']

        for miner in top_mpo:
            data = df.query('block_miner == @miner')
            ax = plot_scatter(x=data.block_height, y=data.tx_intersection, x_label='Block height',
                              y_label=r'$\mid B_i \cap \hat{B}_i \mid$', ax=ax, alpha=.8, label=miner, marker=markers.pop(), size=100)

        legend = ax.legend(ncol=1, loc="upper center",
                           frameon=True, bbox_to_anchor=(0.5, 1.25))
        h, l = ax.get_legend_handles_labels()
        size_lgd = plt.legend([h[0], h[3], h[1], h[4], h[2]],
                              [l[0], l[3], l[1], l[4],  l[2]],
                              ncol=3, loc="upper center", frameon=True, bbox_to_anchor=(0.5, 1.43))

        ax.xaxis.set_minor_locator(ticker.MultipleLocator(100))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

        plt.grid(which='major', axis='both', linestyle='--')
        plt.grid(which='minor', axis='both', linestyle=':')

        plt.xticks(rotation=90)
        fig.set_size_inches(8, 2.7)
        plt.savefig(self.filename_top_mpo_scatter_plot,
                    bbox_inches='tight')

    def __load_dataframe(self):
        df = pd.read_csv(self.filename_dataset, sep=self.sep,
                         compression=self.compression, usecols=self.usecols, parse_dates=self.parse_dates)
        return df
