import numpy as np
import pandas as pd

from .utils import *


class BlockBar:

    def __init__(self):
        self.filename_plot = 'plots/bar.pdf'

    def plot(self):
        plt.clf()
        miners = pd.Series(self.__load_dataframe())
        ax = (100*miners.value_counts(normalize=True)).plot.bar()
        autolabel(ax.patches, precision=0)
        ax.set_ylabel('Miner share (%)')
        ax.set_ylim((0, 100))
        ax.yaxis.set_minor_locator(ticker.MultipleLocator(10))
        plt.xticks(rotation=0)

        plt.savefig(self.filename_plot,
                    bbox_inches='tight')

    def __load_dataframe(self):
        # This is a synthetic dataset
        miners = ['BTC.com']*28
        miners += ['AntPool']*25
        miners += ['F2Pool'] * 20
        miners += ['Poolin.com']*10
        miners += ['SlushPool'] * 9
        miners += ['Others'] * 8
        return miners
