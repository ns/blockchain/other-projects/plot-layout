# Plot layout for scientific papers

## How to run

----------

```bash
make
```

Please, keep in mind that you should have the [Clear Sans](https://www.fontsquirrel.com/fonts/clear-sans) font installed in your system.

## What's New

----------

- Initial commit

## Ask a Question

----------

- For reporting bugs please use the [plot-layout/-/issues](https://gitlab.mpi-sws.org/ns/blockchain/other-projects/plot-layout/-/issues) page.

In case of any issue, please feel free to contact me at johnme@mpi-sws.org

## License

----------

MIT License
