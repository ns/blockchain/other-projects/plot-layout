default:
	python main.py

clean:
	rm -rf src/*.pyc plots
	@find src/ -name "*.pyc" -type f -exec rm '{}' \;
	@find src/ -name "__pycache__" -type d -exec rm -rf '{}' \;

